import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class preprocessing {

    static class Transition {
        int source;
        String event;
        String guardString;
        int target;
        String type;
        String repEvent;
        String repGuardString;
        String tag;
        Set <String> toBuf;
        Transition (int isource, String ievent, String iguardString, int itarget,
        String itype, String irepEvent, String irepGuardString, String itag, Set <String> itoBuf) {
            source = isource;
            event = ievent;
            guardString = iguardString;
            target = itarget;
            type = itype;
            repEvent = irepEvent;
            repGuardString = irepGuardString;
            tag = itag;
            toBuf = itoBuf;
        }
    }

    static void printProp (Map <Integer, Set <Transition>> propMap) {
        for (Integer st : propMap.keySet()) {
            System.out.println("\n\tState " + st);
            for (Transition tr : propMap.get(st)) {
                if (tr.guardString.length() > 0) {
                    System.out.print("\t\tSrc: " + tr.source + ", Label: " + tr.event + "[" +tr.guardString+"]");
                } else {
                    System.out.print("\t\tSrc: " + tr.source + ", Label: " + tr.event);
                }
                System.out.print(", Tgt: " + tr.target);
                System.out.print(", Type: " + tr.type);
                if (tr.repEvent.length()>0) {
                    if (tr.repGuardString.length()>0) {
                        System.out.print(" (" + tr.repEvent + "["+ tr.repGuardString +"])");
                    } else {
                        System.out.print(" (" + tr.repEvent + ")");
                    }
                }
                System.out.println();
            }
        }
    }

    static void printRelease1 (Map <Integer, Set <Transition>> propMap) {
        System.out.println("\nTransitions tagged as release1: ");
        for (Integer st : propMap.keySet()) {
            for (Transition tr : propMap.get(st)) {
                if (tr.tag.equals("release1")) {
                    if (tr.guardString.length() > 0) {
                        System.out.print("Src: " + tr.source + ", Label: " + tr.event + "[" +tr.guardString+"]");
                    } else {
                        System.out.print("Src: " + tr.source + ", Label: " + tr.event);
                    }
                    System.out.print(", Tgt: " + tr.target);
                    System.out.print(", Type: " + tr.type);
                    System.out.print(", Tag: " + tr.tag);
                    if (tr.repEvent.length()>0) {
                        if (tr.repGuardString.length()>0) {
                            System.out.print(" (" + tr.repEvent + "["+ tr.repGuardString +"])");
                        } else {
                            System.out.print(" (" + tr.repEvent + ")");
                        }
                    }
                    System.out.println();
                }
            }
        }
    }

    static void printRelease2 (Map <Integer, Set <Transition>> propMap) {
        System.out.println("\nTransitions tagged as release2: ");
        for (Integer st : propMap.keySet()) {
            for (Transition tr : propMap.get(st)) {
                if (tr.tag.equals("release2")) {
                    if (tr.guardString.length() > 0) {
                        System.out.print("Src: " + tr.source + ", Label: " + tr.event + "[" +tr.guardString+"]");
                    } else {
                        System.out.print("Src: " + tr.source + ", Label: " + tr.event);
                    }
                    System.out.print(", Tgt: " + tr.target);
                    System.out.print(", Type: " + tr.type);
                    System.out.print(", Tag: " + tr.tag);
                    if (tr.repEvent.length()>0) {
                        if (tr.repGuardString.length()>0) {
                            System.out.print(" (" + tr.repEvent + "["+ tr.repGuardString +"])");
                        } else {
                            System.out.print(" (" + tr.repEvent + ")");
                        }
                    }
                    System.out.println();
                }
            }
        }
    }

    static void findBufferLabels (Map <Integer, Set <Transition>> propMap, Set <String> labels, int cState, Set <Integer> visited) {
        visited.add(cState);
        for (Transition propTran : propMap.get(cState)) {
            if (propTran.type.equals("BUFFER")) {
                labels.add(propTran.event + "[" + propTran.guardString+"]");
            } 
            if (!visited.contains(propTran.target)) {
                findBufferLabels (propMap, labels, propTran.target, visited);
            }
        }
    }

    static void findRelease2 (Map <Integer, Set <Transition>> propMap, Set <String> labels, Set <Integer> release2States, int cState, Set <Integer> visited) {
        visited.add(cState);
        for (Transition propTran : propMap.get(cState)) {
            String label = propTran.event + "[" + propTran.guardString + "]";
            if (labels.contains(label) && propTran.type.equals("FORWARD")) {
                propTran.tag = "release2";
                if (propTran.target != propTran.source) {
                    release2States.add(propTran.target);
                }
            } 
            if (!visited.contains(propTran.target)) {
                findRelease2 (propMap, labels, release2States, propTran.target, visited);
            }
        }
    }

    static void modRelease2 (Map <Integer, Set <Transition>> propMap, Set <Integer> release2States, int cState, Set <Integer> visited) {
        visited.add(cState);
        for (Transition propTran : propMap.get(cState)) {
            if (propTran.tag.equals("release2") && release2States.contains(propTran.source)) {
                propTran.tag = "";
            } 
            if (!visited.contains(propTran.target)) {
                modRelease2 (propMap, release2States, propTran.target, visited);
            }
        }
    }

    static void findStatesRelease1 (Map <Integer, Set <Transition>> propMap, Set <Integer> release1States, int cState, Set <Integer> visited) {
        visited.add(cState);
        for (Transition propTran : propMap.get(cState)) {
            if (propTran.tag.equals("release2")) {
                release1States.add(propTran.source);
            } 
            if (!visited.contains(propTran.target)) {
                findStatesRelease1 (propMap, release1States, propTran.target, visited);
            }
        }
    }

    static void findRelease1 (Map <Integer, Set <Transition>> propMap, Set <Integer> release1States, int cState, Set <Integer> visited) {
        visited.add(cState);
        for (Transition propTran : propMap.get(cState)) {
            if (release1States.contains(propTran.target) && propTran.target != propTran.source) {
                propTran.tag = "release1";
                Set <String> tmpToBuf = new HashSet<String>();
                tmpToBuf.addAll(propTran.toBuf);
                for (Transition tmpTran : propMap.get(propTran.target)) {
                    if (tmpTran.tag.equals("release2")) {
                        tmpToBuf.add(tmpTran.event);
                    }
                }
                propTran.toBuf.addAll(tmpToBuf);
            } 
            if (!visited.contains(propTran.target)) {
                findRelease1 (propMap, release1States, propTran.target, visited);
            }
        }
    }

    static void propToTxt (Map <Integer, Set <Transition>> propMap, String propName, String enfName) {
        try {
            FileWriter myWriter = new FileWriter(propName);
            myWriter.write(enfName+"\n");
            for (int st : propMap.keySet()) {
                for (Transition tr : propMap.get(st)) {
                    myWriter.write(tr.source + ", " +tr.event);
                    if (tr.guardString.length() > 0) {
                        myWriter.write("[" + tr.guardString + "]");
                    }
                    myWriter.write("." + tr.type);
                    if(tr.type.equals("REPLACE") && tr.repEvent.length() > 0) {
                        myWriter.write("."+tr.repEvent);
                        if (tr.repGuardString.length() > 0) {
                            myWriter.write("[" + tr.repGuardString + "]");
                        }   
                    }
                    myWriter.write(", " + tr.target);
                    if (tr.tag.length() > 0) {
                        myWriter.write(" | " + tr.tag);
                    } else {
                        myWriter.write(" | ");
                    }
                    if (tr.toBuf.size() > 0){
                        myWriter.write((" ("));
                        int counter = 1;
                        for (String tb : tr.toBuf) {
                            if (counter < tr.toBuf.size()) {
                                myWriter.write(tb + ",");
                            } else {
                                myWriter.write(tb);
                            }   
                            counter++;
                        }
                        myWriter.write((")"));
                    }
                    myWriter.write("\n");
                }
            }
            myWriter.close();
            System.out.println("Successfully wrote to the file: " + propName);
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws FileNotFoundException, IOException {
        String fileName = args[0];
        String propMeta = "";
        int tmpSource = 0;
        String tmpEvent = "";
        String tmpGuardString = "";
        int tmpTarget = 0;
        String tmpType = "";
        String tmpRepEvent = "";
        String [] tmpLabel;
        String [] tmpRepLabel;
        String tmpRepGuardString = "";
        Set <Transition> tmpTrans;
        String[] arrLines;
        
        Set <String> labels = new HashSet<String>();
        Set <Integer> release2States = new HashSet<Integer>();
        Set <Integer> release1States = new HashSet<Integer>();

        Map <Integer, Set <Transition>> propMap = new HashMap <Integer, Set <Transition>>();
        try(BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String line = br.readLine();
            propMeta = line;
            while (true) {
                line = br.readLine();
                if (line != null){
                    arrLines = line.split(",");
                    tmpLabel = arrLines[1].split("\\.");
                    tmpSource = Integer.parseInt(arrLines[0]);
                    tmpEvent = tmpLabel[0].split("\\[")[0].replace(" ", "");
                    
                    tmpGuardString = "";
                    if (tmpLabel[0].split("\\[").length>1) {
                        tmpGuardString = tmpLabel[0].split("\\[")[1].replace("]", "");
                    }
                    tmpTarget = Integer.parseInt(arrLines[2].replace(" ", ""));
                    tmpTrans = new HashSet<Transition>();         
                    if (propMap.containsKey(tmpSource)) {
                        tmpTrans.addAll(propMap.get(tmpSource));
                    }
                    tmpType = "";
                    tmpType = tmpLabel[1].replace(" ", "");

                    tmpRepEvent = "";
                    tmpRepGuardString = "";
                    if (tmpLabel.length > 2) {
                        tmpRepLabel = tmpLabel[2].split("\\[");
                        tmpRepEvent = tmpRepLabel[0].replace(" ", "");
                        if(tmpLabel[2].split("\\[").length > 1) {
                            tmpRepGuardString = tmpRepLabel[1].replace("]", "");
                        }
                    }

                    tmpTrans.add(new Transition(tmpSource, tmpEvent, tmpGuardString, tmpTarget, tmpType, tmpRepEvent, tmpRepGuardString, "", new HashSet<String>()));
                    propMap.put(tmpSource, tmpTrans);

                    if(!propMap.containsKey(tmpTarget)) {
                        propMap.put(tmpTarget, new HashSet<Transition>());
                    }
                } else { break; }
            }
        }

        System.out.println("============================================\nINPUT\n");
        System.out.println("Property Name: " + propMeta);
        printProp(propMap);

        // correcting
        

        // tagging
        findBufferLabels(propMap, labels, 0, new HashSet<Integer>());
        findRelease2(propMap, labels, release2States, 0, new HashSet<Integer>());
        modRelease2(propMap, release2States, 0, new HashSet<Integer>());
        findStatesRelease1(propMap, release1States, 0, new HashSet<Integer>());
        findRelease1(propMap, release1States, 0, new HashSet<Integer>());

        // final
        System.out.println("\n============================================\nOutput");
        printRelease1(propMap);
        printRelease2(propMap);
        
        propToTxt(propMap, fileName.split("\\.")[0]+"-prime.txt", propMeta);
    }
}
