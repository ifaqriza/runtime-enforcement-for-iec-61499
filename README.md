# Runtime Enforcement for IEC 61499
This repository contains the implementation of runtime enforcement for industrial automation based on IEC 61499 standard.
Our implementation relies on 4DIAC-IDE (https://www.eclipse.org/4diac/index.php), therefore, it is mandatory to install it before testing the enforcers via simulation.

## Directories
- CaseStudy-with-Enforcers and CaseStudy-without-Enforcers: the quality test station case studies, respectively, with and without enforcers.
- Enforcers: the synthesised enforcers that are presented in the paper.
- Experiments: the projects that we use for testing the impact of enforcers in terms of performance.
- Synthesiser: the tool support and examples with explanations on how to use it.