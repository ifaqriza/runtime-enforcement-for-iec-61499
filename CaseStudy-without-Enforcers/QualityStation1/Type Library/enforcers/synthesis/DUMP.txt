// static void buildECCForward(Map <Integer, Set <Transition>> propMapIn, EnforcerState cStateEnf, int cState,
// Map <EnforcerState, Set <EnforcerTransition>> eccOut, Set <Integer> visited, Map <Integer, EnforcerState> mapStateEnf,
// Set <EnforcerAlgo> algosOut) {
//     visited.add(cState);
//     mapStateEnf.put(cState, cStateEnf);
    
//     for (Transition propTran : propMapIn.get(cState)) {
//         if (propTran.type.equals("FORWARD")) {

//             // create main transition label
//             String transEvIn = propTran.event + "_I";

//             Iterator <String> it = propTran.guard.iterator();
//             String tmpGuard = "";
//             String tmpEnfAlgo = "";
//             Set <EnforcerAction> targetActs = new HashSet<EnforcerAction>();
//             while (it.hasNext()) { 
//                 tmpGuard = it.next(); 
//                 if (tmpGuard.contains("!=")) {
//                     tmpEnfAlgo += tmpGuard.split("!=")[0] + "_O := " + tmpGuard.split("!=")[0] + "_I; ";
//                 } else if (tmpGuard.contains("<=")) {
//                     tmpEnfAlgo += tmpGuard.split("<=")[0] + "_O := " + tmpGuard.split("<=")[0] + "_I; ";
//                 } else if (tmpGuard.contains(">=")) {
//                     tmpEnfAlgo += tmpGuard.split(">=")[0] + "_O := " + tmpGuard.split(">=")[0] + "_I; ";
//                 } else if (tmpGuard.contains("=")) {
//                     tmpEnfAlgo += tmpGuard.split("=")[0] + "_O := " + tmpGuard.split("=")[0] + "_I; ";
//                 } else if (tmpGuard.contains("<")) {
//                     tmpEnfAlgo += tmpGuard.split("<")[0] + "_O := " + tmpGuard.split("<")[0] + "_I; ";
//                 } else if (tmpGuard.contains(">")) {
//                     tmpEnfAlgo += tmpGuard.split(">")[0] + "_O := " + tmpGuard.split(">")[0] + "_I; ";
//                 }
//             }
//             Set <EnforcerAlgo> tmpExistAlgo = new HashSet<EnforcerAlgo>();
//             for (EnforcerAlgo enforcerAlgo : algosOut) {
//                 if (enforcerAlgo.algo.equals(tmpEnfAlgo)) {
//                     tmpExistAlgo.add(enforcerAlgo);
//                 }
//             }
//             EnforcerAlgo newEnfAlgo;
//             String targetEvOut = propTran.event + "_O";
//             EnforcerAction targetAct;
//             if (tmpEnfAlgo.length() > 0) {
//                 if (tmpExistAlgo.size() == 0) {
//                     newEnfAlgo = new EnforcerAlgo(tmpEnfAlgo);
//                     algosOut.add(newEnfAlgo);
//                 } else {
//                     Iterator <EnforcerAlgo> itAlgo = tmpExistAlgo.iterator();
//                     newEnfAlgo = itAlgo.next();
//                 }
//                 targetAct = new EnforcerAction(newEnfAlgo.ID, targetEvOut);
//             } else {
//                 targetAct = new EnforcerAction("", targetEvOut);
//             }
//             targetActs.add(targetAct);

//             String guardString = propTran.guardString.replaceAll(" = ", "_I = ")
//                                                     .replaceAll(" != ", "_I != ")
//                                                     .replaceAll(" <= ", "_I <= ")
//                                                     .replaceAll(" >= ", "_I >= ")
//                                                     .replaceAll(" > ", "_I > ")
//                                                     .replaceAll(" < ", "_I < ");

//             // create main transition target state
//             EnforcerState targetMain = new EnforcerState(targetActs);

//             // create main transition (not empty); then, insert to ecc
//             EnforcerTransition transMain = new EnforcerTransition(cStateEnf, transEvIn, guardString, propTran.guardString, targetMain, false);
//             Set <EnforcerTransition> transMains = new HashSet<EnforcerTransition>();
//             if (eccOut.containsKey(cStateEnf)) {
//                 transMains.addAll(eccOut.get(cStateEnf));
//             }
//             transMains.add(transMain);
//             eccOut.put(cStateEnf, transMains);

//             // create second transition (empty, from state with, to state without actions)
//             Set <EnforcerAction> targetActsS = new HashSet<EnforcerAction>();
//             if (!visited.contains(propTran.target)) {
//                 EnforcerState targetSecond = new EnforcerState(targetActsS);
//                 EnforcerTransition transSecond = new EnforcerTransition(targetMain, targetSecond, true);
//                 Set <EnforcerTransition> transSeconds = new HashSet<EnforcerTransition>();
//                 transSeconds.add(transSecond);
//                 eccOut.put(targetMain, transSeconds);
//                 buildECCForward(propMapIn, targetSecond, propTran.target, eccOut, visited, mapStateEnf, algosOut);
//             } else {
//                 EnforcerTransition transSecond = new EnforcerTransition(targetMain, mapStateEnf.get(propTran.target), true);
//                 Set <EnforcerTransition> transSeconds = new HashSet<EnforcerTransition>();
//                 transSeconds.add(transSecond);
//                 eccOut.put(targetMain, transSeconds);
//             }
//         }
//     }
// }

// static void buildECCReplace (Map <Integer, Set <Transition>> propMapIn, Map <EnforcerState, Set <EnforcerTransition>> eccOut,
//     Map<Integer, EnforcerState> mapStates, Set <EnforcerAlgo> algosOut) {
    
    
//     for (Integer st : mapStates.keySet()) {
//         for (Transition tran : propMapIn.get(st)) {
//             if (tran.type.equals("REPLACE")) {
//                 Set <EnforcerState> tmpDstState = new HashSet<EnforcerState>();
//                 // get final state
//                 for (EnforcerTransition trEf : eccOut.get(mapStates.get(st))) {
//                     if (trEf.event.equals(tran.repEvent+"_I") && trEf.propGuardString.equals(tran.repGuardString)) {
//                         tmpDstState.add(trEf.target);
//                     }
//                 }
//                 EnforcerState stFinal = eccOut.get(tmpDstState.iterator().next()).iterator().next().target;
                
//                 // create target state
//                 Iterator <String> it = tran.repGuard.iterator();
//                 String tmpGuard = "";
//                 String tmpEnfAlgo = "";
//                 Set <EnforcerAction> targetActs = new HashSet<EnforcerAction>();
//                 while (it.hasNext()) { 
//                     tmpGuard = it.next(); 
//                     tmpEnfAlgo += tmpGuard.split("=")[0] + "_O := " + tmpGuard.split("=")[1] + "; ";
//                 }
//                 Set <EnforcerAlgo> tmpExistAlgo = new HashSet<EnforcerAlgo>();
//                 for (EnforcerAlgo enforcerAlgo : algosOut) {
//                     if (enforcerAlgo.algo.equals(tmpEnfAlgo)) {
//                         tmpExistAlgo.add(enforcerAlgo);
//                     }
//                 }
//                 EnforcerAlgo newEnfAlgo;
//                 String targetEvOut = tran.repEvent + "_O";
//                 EnforcerAction targetAct;
//                 if (tmpEnfAlgo.length() > 0) {
//                     if (tmpExistAlgo.size() == 0) {
//                         newEnfAlgo = new EnforcerAlgo(tmpEnfAlgo);
//                         algosOut.add(newEnfAlgo);
//                     } else {
//                         Iterator <EnforcerAlgo> itAlgo = tmpExistAlgo.iterator();
//                         newEnfAlgo = itAlgo.next();
//                     }
//                     targetAct = new EnforcerAction(newEnfAlgo.ID, targetEvOut);
//                 } else {
//                     targetAct = new EnforcerAction("", targetEvOut);
//                 }
//                 targetActs.add(targetAct);
//                 EnforcerState srcState = mapStates.get(st);
//                 EnforcerState newTargetState = new EnforcerState(targetActs);

//                 // create transitions
//                 String guardString = tran.guardString.replaceAll(" = ", "_I = ")
//                                                             .replaceAll(" != ", "_I != ")
//                                                             .replaceAll(" <= ", "_I <= ")
//                                                             .replaceAll(" >= ", "_I >= ")
//                                                             .replaceAll(" > ", "_I > ")
//                                                             .replaceAll(" < ", "_I < ");
//                 EnforcerTransition newMainTran = new EnforcerTransition(srcState, tran.event+"_I", guardString, tran.guardString, newTargetState, false);
//                 Set <EnforcerTransition> tmpTrans = eccOut.get(srcState);
//                 tmpTrans.add(newMainTran);
//                 eccOut.put(srcState, tmpTrans);
//                 EnforcerTransition newSecondTran = new EnforcerTransition(newTargetState, stFinal, true);
//                 Set <EnforcerTransition> tmpSecondTrans = new HashSet<EnforcerTransition>();
//                 tmpSecondTrans.add(newSecondTran);
//                 eccOut.put(newTargetState, tmpSecondTrans);
//             }
//         }
//     }
// }

// static void buildECCBuffer (Map <Integer, Set <Transition>> propMapIn, Map <EnforcerState, Set <EnforcerTransition>> eccOut,
//     Map<Integer, EnforcerState> mapStates, Set <EnforcerAlgo> algosOut, Set <String> iVars) {
    
//     // create transitions and states for adding to buffer
//     for (Integer st : mapStates.keySet()) {
//         for (Transition tran : propMapIn.get(st)) {
//             if (tran.type.equals("BUFFER")) {
//                 // create target state
//                 String tmpEnfAlgo = "Buffer_" + tran.event + " := Buffer_" + tran.event + " + 1;";
//                 Set <EnforcerAlgo> tmpExistAlgo = new HashSet<EnforcerAlgo>();
//                 for (EnforcerAlgo enforcerAlgo : algosOut) {
//                     if (enforcerAlgo.algo.equals(tmpEnfAlgo)) {
//                         tmpExistAlgo.add(enforcerAlgo);
//                     }
//                 }
//                 EnforcerAlgo newEnfAlgo;
//                 if (tmpExistAlgo.size() == 0) {
//                     newEnfAlgo = new EnforcerAlgo(tmpEnfAlgo);
//                     algosOut.add(newEnfAlgo);
//                 } else {
//                     Iterator <EnforcerAlgo> itAlgo = tmpExistAlgo.iterator();
//                     newEnfAlgo = itAlgo.next();
//                 }
//                 EnforcerAction act = new EnforcerAction(newEnfAlgo.ID, "");
//                 Set <EnforcerAction> acts = new HashSet<EnforcerAction>();
//                 acts.add(act);
//                 EnforcerState target = new EnforcerState(acts);
                
//                 // create first transition
//                 Set <EnforcerTransition> tmpTrans = eccOut.get(mapStates.get(st));
//                 tmpTrans.add(new EnforcerTransition(mapStates.get(st), tran.event + "_I", "", 
//                     "", target, false));
//                 eccOut.put(mapStates.get(st), tmpTrans);

//                 // create second transition
//                 Set <EnforcerTransition> tmpSecondTrans = new HashSet<EnforcerTransition>();
//                 EnforcerTransition secondTran = new EnforcerTransition(target, mapStates.get(st), true);
//                 tmpSecondTrans.add(secondTran);
//                 eccOut.put(target, tmpSecondTrans);
//             }
//         }
//     }

//     // create transition and states for releasing from buffer
//     Set <String> bufferEvents = new HashSet<String>();
//     for (String iVar : iVars) {
//         bufferEvents.add(iVar.split("_")[1]);
//     }
//     for (String bufEv : bufferEvents) {
//         Set <BufferSourceEventTarget> toModif = new HashSet<BufferSourceEventTarget>();

//         for (Integer propSt : propMapIn.keySet()) {
//             for (Transition propTran : propMapIn.get(propSt)) {
//                 if (propTran.event.equals(bufEv) && propTran.type.equals("FORWARD")) {
//                     for (Integer stToEv : mapStates.keySet()) {
//                         for (Transition trToEv : propMapIn.get(stToEv)) {
//                             if (trToEv.target == propTran.source) {
                                
//                                 Set<EnforcerTransition> tgts = eccOut.get(mapStates.get(propTran.source));
//                                 EnforcerTransition tgt = tgts.iterator().next();
//                                 toModif.add(new BufferSourceEventTarget(mapStates.get(trToEv.source), trToEv, tgt.target));
//                             }
//                         }
//                     }
//                 }
//             }
//         }

//         for (BufferSourceEventTarget tm : toModif) {
//             System.out.println("src: " + tm.src.state + ", ev and gd: " + tm.tr.event +" "+ tm.tr.guardString + ", tgt: " + tm.tgt.state);
//             // create new state
//             String tmpEnfAlgo = "Buffer_" + bufEv + " := Buffer_" + bufEv + " - 1;";
//             Set <EnforcerAlgo> tmpExistAlgo = new HashSet<EnforcerAlgo>();
//             for (EnforcerAlgo enforcerAlgo : algosOut) {
//                 if (enforcerAlgo.algo.equals(tmpEnfAlgo)) {
//                     tmpExistAlgo.add(enforcerAlgo);
//                 }
//             }
//             EnforcerAlgo newEnfAlgo;
//             if (tmpExistAlgo.size() == 0) {
//                 newEnfAlgo = new EnforcerAlgo(tmpEnfAlgo);
//                 algosOut.add(newEnfAlgo);
//             } else {
//                 Iterator <EnforcerAlgo> itAlgo = tmpExistAlgo.iterator();
//                 newEnfAlgo = itAlgo.next();
//             }
//             EnforcerAction act = new EnforcerAction(newEnfAlgo.ID, tm.tr.event + "_O");
//             Set <EnforcerAction> acts = new HashSet<EnforcerAction>();
//             acts.add(act);
//             EnforcerState mainTarget = new EnforcerState(acts);

//             // add main tran
//             String mainGuardStr = "Buffer_" + bufEv + " > 0";
//             EnforcerTransition mainTran = new EnforcerTransition(tm.src, tm.tr.event + "_I", mainGuardStr, "", mainTarget, false);
//             Set <EnforcerTransition> mainTrans = eccOut.get(tm.src);
//             mainTrans.add(mainTran);
//             eccOut.put(tm.src, mainTrans);

//             // add second tran
//             EnforcerTransition secondTran = new EnforcerTransition(mainTarget, tm.tgt, false);
//             Set <EnforcerTransition> scTrans = new HashSet<EnforcerTransition>();
//             scTrans.add(secondTran);
//             eccOut.put(mainTarget, scTrans);

//             for (EnforcerTransition tranModif : eccOut.get(tm.src)) {
//                 if (tranModif.event.equals(tm.tr.event + "_I") && tranModif.propGuardString.equals(tm.tr.guardString) && tranModif.target.state != mainTarget.state) {
//                     tranModif.guardString += "Buffer_" + bufEv + " = 0";
//                 }
//             }
//         }
//     }
// }