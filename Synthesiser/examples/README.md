# Runtime Enforcement for IEC 61499
This directory contains examples of contract automata and enforcers.
It also provides explanation on how to user the synthesiser tool.
Note that Java 11 is required.
Also, the generated enforcer is in XML format;
we refer to 4DIAC's official documentation https://www.eclipse.org/4diac/en_help.php?helppage=html/installation/install.html in order to compile and simulate it in 4DIAC-IDE.

## Command
java -cp synthesiser.jar enforcer [automaton file in txt format]

## Example
There is an example of an enforcer in this directory called AutomatonOne.fbt.
This FB is synthesised from the contract automaton specified in AutomatonOne.txt.
In this case, the command is:
java -cp synthesiser.jar enforcer AutomatonOne.txt.

## How to write the automaton
As shown in AutomatonOne.txt, the first line of the text file is the name of the automaton, which also will be the name of the enforcer.
The set of transitions starts from the second line.
The syntax is slightly different from what we have seen in the paper.
Here: the separator between elements is a dot, the sequence of replacements data (for transition typed as replace) is inside a square bracket, uses "=" as assignments (instead of :=), and separated by "AND".