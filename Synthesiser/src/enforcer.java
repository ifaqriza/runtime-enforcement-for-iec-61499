import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.io.File;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

class enforcer {

    static class Transition {
        int source;
        String event;
        Set <String> guard;
        String guardString;
        int target;
        String type;
        String repEvent;
        Set <String> repGuard;
        String repGuardString;
        Set <String> data;
        String tag;
        Set <String> toBuf;
        Transition (int isource, String ievent, Set <String> iguard, String iguardString, int itarget, String itype, String irepEvent, Set <String> irepGuard, String irepGuardString, Set <String> idata, String itag, Set <String> itoBuf) {
            source = isource;
            event = ievent;
            guard = iguard;
            guardString = iguardString;
            target = itarget;
            type = itype;
            repEvent = irepEvent;
            repGuard = irepGuard;
            repGuardString = irepGuardString;
            data = idata;
            tag = itag;
            toBuf = itoBuf;
        }
    }
    static class With {
        String event;
        String data;
        With (String iEvent, String iData){
            event = iEvent;
            data = iData;
        }
    
        @Override
        public int hashCode() {
          final int prime = 31;
          int result = 1;
          result = prime * result + ((event == null) ? 0 : event.hashCode()) + ((data == null) ? 0 : data.hashCode());
          return result;
        }
    
        @Override
        public boolean equals(Object o) {
            With tmpWith = (With) o;
            if (event.equals(tmpWith.event) && data.equals(tmpWith.data)) {
                return true;
            } else {
                return false;
            }
        }
    }
    
    static class EnforcerFB {
        String name;
        EnforcerInterfaces interfaces;
        Map <EnforcerState, Set<EnforcerTransition>> ecc;
        Set <EnforcerAlgo> algorithms;
    
        EnforcerFB (String iName, EnforcerInterfaces iInterfaces,
                    Map <EnforcerState, Set<EnforcerTransition>> iEcc, Set <EnforcerAlgo> iAlgorithms) {
            name =  iName;
            interfaces = iInterfaces;
            ecc = iEcc;
            algorithms = iAlgorithms;
        }
    }
    
    static class EnforcerInterfaces {
        Set <String> eventInputs;
        Set <String> eventOutputs;
        Set <dataItf> dataInputs;
        Set <dataItf> dataOutputs;
        Set <With> withInputs;
        Set <With> withOutputs;
        EnforcerInterfaces (Set <String> iEventInputs, Set <String> iEventOutputs,
                            Set <dataItf> iDataInputs, Set <dataItf> iDataOutputs,
                            Set <With> iWithInputs, Set <With> iWithOutputs) {
            eventInputs = iEventInputs;
            eventOutputs = iEventOutputs;
            dataInputs = iDataInputs;
            dataOutputs = iDataOutputs;
            withInputs = iWithInputs;
            withOutputs = iWithOutputs;
        }
    }
    
    static class dataItf {
        String name;
        String type;
        dataItf (String iName, String iType) {
            name = iName;
            type = iType;
        }

        @Override
        public int hashCode() {
            return Objects.hash(name, type);
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                { return true; }
            if (obj == null || getClass() != obj.getClass())
                { return false; }
            dataItf other = (dataItf) obj;
            return Objects.equals(name, other.name) &&
                    Objects.equals(type, other.type);
        }
    }

    static class iVariables {
        String name;
        String type;
        Boolean isEvent;
        iVariables (String iName, String iType, Boolean iIsEvent) {
            name = iName;
            type = iType;
            isEvent = iIsEvent;
        }

        @Override
        public int hashCode() {
            return Objects.hash(name, type, isEvent);
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                { return true; }
            if (obj == null || getClass() != obj.getClass())
                { return false; }
            iVariables other = (iVariables) obj;
            return Objects.equals(name, other.name) &&
                    Objects.equals(type, other.type) &&
                    Objects.equals(isEvent, other.isEvent);
        }
    }
    
    static class EnforcerAlgo {
        private static final AtomicInteger idx = new AtomicInteger(0);
        String ID;
        String algo;
        EnforcerAlgo (String iAlgo) {
            int id = idx.incrementAndGet();
            ID = "Algo"+id;
            algo = iAlgo;
        }
    }
    
    static class EnforcerAction {
        String algoID;
        String eventOut;
        EnforcerAction (String iAlgoID, String iEventOut) {
            algoID = iAlgoID;
            eventOut = iEventOut;
        }
    }
    
    static class  EnforcerState {
        private static final AtomicInteger idx = new AtomicInteger(-1); 
        int state;
        List <EnforcerAction> actions;
        EnforcerState (List <EnforcerAction> iActions) {
            state = idx.incrementAndGet();
            actions = iActions;
        }
        
        @Override
        public boolean equals(Object o) {
            EnforcerState tmpState = (EnforcerState) o;
            if (state == tmpState.state) {
                return true;
            } else {
                return false;
            }
        }
    }
    
    static class EnforcerTransition {
        EnforcerState source;
        String event;
        String guardString;
        String propGuardString;
        EnforcerState target;
        boolean isEmpty;
        EnforcerTransition (EnforcerState isource, EnforcerState itarget, boolean iIsEmpty) {
            source = isource;
            event = "";
            guardString = "";
            propGuardString = "";
            target = itarget;
            isEmpty = iIsEmpty;
        }
        EnforcerTransition (EnforcerState isource, String ievent, String iguardString, String ipropGuardString, EnforcerState itarget, boolean iIsEmpty) {
            source = isource;
            event = ievent;
            guardString = iguardString;
            propGuardString = ipropGuardString;
            target = itarget;
            isEmpty = iIsEmpty;
        }
    }

    static class BufferSourceEventTarget {
        EnforcerState src;
        Transition tr;
        EnforcerState tgt;
        BufferSourceEventTarget (EnforcerState isrc, Transition itr, EnforcerState itgt) {
            src = isrc;
            tr = itr;
            tgt = itgt;
        }
    }

    static void buildAllECC(Map <Integer, Set <Transition>> propMapIn, EnforcerState cStateEnf, int cState,
    Map <EnforcerState, Set <EnforcerTransition>> eccOut, Set <Integer> visited, Map <Integer, EnforcerState> mapStateEnf,
    Set <EnforcerAlgo> algosOut) {
        visited.add(cState);
        mapStateEnf.put(cState, cStateEnf);
        for (Transition propTran : propMapIn.get(cState)) {
            
            String guardString = propTran.guardString.replaceAll(" = ", "_I = ")
            .replaceAll(" != ", "_I != ")
            .replaceAll(" <= ", "_I <= ")
            .replaceAll(" >= ", "_I >= ")
            .replaceAll(" > ", "_I > ")
            .replaceAll(" < ", "_I < ");
            if(propTran.tag.equals("release1")){
                for (String tb : propTran.toBuf) {
                    if (guardString.equals("")) {
                        guardString += "Buf_" +tb+ " = 0" ;
                    } else {
                        guardString += " AND Buf_" +tb+ " = 0" ;
                    }
                }
            }
            String transEvIn = propTran.event + "_I";
            List <EnforcerAction> targetActs = new ArrayList<EnforcerAction>();
            String tmpEnfAlgo = "";
            String tmpGuard = "";
            if (propTran.type.equals("FORWARD")) {
                Iterator <String> it = propTran.guard.iterator();
                while (it.hasNext()) { 
                    tmpGuard = it.next(); 
                    if (tmpGuard.contains("!=")) {
                        tmpEnfAlgo += tmpGuard.split("!=")[0] + "_O := " + tmpGuard.split("!=")[0] + "_I; ";
                    } else if (tmpGuard.contains("<=")) {
                        tmpEnfAlgo += tmpGuard.split("<=")[0] + "_O := " + tmpGuard.split("<=")[0] + "_I; ";
                    } else if (tmpGuard.contains(">=")) {
                        tmpEnfAlgo += tmpGuard.split(">=")[0] + "_O := " + tmpGuard.split(">=")[0] + "_I; ";
                    } else if (tmpGuard.contains("=")) {
                        tmpEnfAlgo += tmpGuard.split("=")[0] + "_O := " + tmpGuard.split("=")[0] + "_I; ";
                    } else if (tmpGuard.contains("<")) {
                        tmpEnfAlgo += tmpGuard.split("<")[0] + "_O := " + tmpGuard.split("<")[0] + "_I; ";
                    } else if (tmpGuard.contains(">")) {
                        tmpEnfAlgo += tmpGuard.split(">")[0] + "_O := " + tmpGuard.split(">")[0] + "_I; ";
                    }
                }
                Set <EnforcerAlgo> tmpExistAlgo = new HashSet<EnforcerAlgo>();
                for (EnforcerAlgo enforcerAlgo : algosOut) {
                    if (enforcerAlgo.algo.equals(tmpEnfAlgo)) {
                        tmpExistAlgo.add(enforcerAlgo);
                    }
                }
                EnforcerAlgo newEnfAlgo;
                String targetEvOut = propTran.event + "_O";
                EnforcerAction targetAct;
                if (tmpEnfAlgo.length() > 0) {
                    if (tmpExistAlgo.size() == 0) {
                        newEnfAlgo = new EnforcerAlgo(tmpEnfAlgo);
                        algosOut.add(newEnfAlgo);
                    } else {
                        Iterator <EnforcerAlgo> itAlgo = tmpExistAlgo.iterator();
                        newEnfAlgo = itAlgo.next();
                    }
                    targetAct = new EnforcerAction(newEnfAlgo.ID, targetEvOut);
                } else {
                    targetAct = new EnforcerAction("", targetEvOut);
                }
                targetActs.add(targetAct);
                
                EnforcerState targetMain = new EnforcerState(targetActs);
                EnforcerTransition transMain = new EnforcerTransition(cStateEnf, transEvIn, guardString, propTran.guardString, targetMain, false);
                Set <EnforcerTransition> transMains = new HashSet<EnforcerTransition>();
                if (eccOut.containsKey(cStateEnf)) {
                    transMains.addAll(eccOut.get(cStateEnf));
                }
                transMains.add(transMain);
                eccOut.put(cStateEnf, transMains);
                List <EnforcerAction> targetActsS = new ArrayList<EnforcerAction>();
                if (!visited.contains(propTran.target)) {
                    EnforcerState targetSecond = new EnforcerState(targetActsS);
                    EnforcerTransition transSecond = new EnforcerTransition(targetMain, targetSecond, true);
                    Set <EnforcerTransition> transSeconds = new HashSet<EnforcerTransition>();
                    transSeconds.add(transSecond);
                    eccOut.put(targetMain, transSeconds);
                    buildAllECC(propMapIn, targetSecond, propTran.target, eccOut, visited, mapStateEnf, algosOut);
                } else {
                    EnforcerTransition transSecond = new EnforcerTransition(targetMain, mapStateEnf.get(propTran.target), true);
                    Set <EnforcerTransition> transSeconds = new HashSet<EnforcerTransition>();
                    transSeconds.add(transSecond);
                    eccOut.put(targetMain, transSeconds);
                }
            } else if (propTran.type.equals("DISCARD")) {
                EnforcerState targetMain = new EnforcerState(targetActs);
                EnforcerTransition transMain = new EnforcerTransition(cStateEnf, transEvIn, guardString, propTran.guardString, targetMain, false);
                Set <EnforcerTransition> transMains = new HashSet<EnforcerTransition>();
                if (eccOut.containsKey(cStateEnf)) {
                    transMains.addAll(eccOut.get(cStateEnf));
                }
                transMains.add(transMain);
                eccOut.put(cStateEnf, transMains);
                List <EnforcerAction> targetActsS = new ArrayList<EnforcerAction>();
                if (!visited.contains(propTran.target)) {
                    EnforcerState targetSecond = new EnforcerState(targetActsS);
                    EnforcerTransition transSecond = new EnforcerTransition(targetMain, targetSecond, true);
                    Set <EnforcerTransition> transSeconds = new HashSet<EnforcerTransition>();
                    transSeconds.add(transSecond);
                    eccOut.put(targetMain, transSeconds);
                    buildAllECC(propMapIn, targetSecond, propTran.target, eccOut, visited, mapStateEnf, algosOut);
                } else {
                    EnforcerTransition transSecond = new EnforcerTransition(targetMain, mapStateEnf.get(propTran.target), true);
                    Set <EnforcerTransition> transSeconds = new HashSet<EnforcerTransition>();
                    transSeconds.add(transSecond);
                    eccOut.put(targetMain, transSeconds);
                }
            } else if (propTran.type.equals("REPLACE")) {
                Iterator <String> it = propTran.repGuard.iterator();
                while (it.hasNext()) { 
                    tmpGuard = it.next(); 
                    tmpEnfAlgo += tmpGuard.split("=")[0] + "_O := " + tmpGuard.split("=")[1] + "; ";
                }
                Set <EnforcerAlgo> tmpExistAlgo = new HashSet<EnforcerAlgo>();
                for (EnforcerAlgo enforcerAlgo : algosOut) {
                    if (enforcerAlgo.algo.equals(tmpEnfAlgo)) {
                        tmpExistAlgo.add(enforcerAlgo);
                    }
                }
                EnforcerAlgo newEnfAlgo;
                String targetEvOut = propTran.repEvent + "_O";
                EnforcerAction targetAct;
                if (tmpEnfAlgo.length() > 0) {
                    if (tmpExistAlgo.size() == 0) {
                        newEnfAlgo = new EnforcerAlgo(tmpEnfAlgo);
                        algosOut.add(newEnfAlgo);
                    } else {
                        Iterator <EnforcerAlgo> itAlgo = tmpExistAlgo.iterator();
                        newEnfAlgo = itAlgo.next();
                    }
                    targetAct = new EnforcerAction(newEnfAlgo.ID, targetEvOut);
                } else {
                    targetAct = new EnforcerAction("", targetEvOut);
                }
                targetActs.add(targetAct);
                
                EnforcerState targetMain = new EnforcerState(targetActs);
                EnforcerTransition transMain = new EnforcerTransition(cStateEnf, transEvIn, guardString, propTran.guardString, targetMain, false);
                Set <EnforcerTransition> transMains = new HashSet<EnforcerTransition>();
                if (eccOut.containsKey(cStateEnf)) {
                    transMains.addAll(eccOut.get(cStateEnf));
                }
                transMains.add(transMain);
                eccOut.put(cStateEnf, transMains);
                List <EnforcerAction> targetActsS = new ArrayList<EnforcerAction>();
                if (!visited.contains(propTran.target)) {
                    EnforcerState targetSecond = new EnforcerState(targetActsS);
                    EnforcerTransition transSecond = new EnforcerTransition(targetMain, targetSecond, true);
                    Set <EnforcerTransition> transSeconds = new HashSet<EnforcerTransition>();
                    transSeconds.add(transSecond);
                    eccOut.put(targetMain, transSeconds);
                    buildAllECC(propMapIn, targetSecond, propTran.target, eccOut, visited, mapStateEnf, algosOut);
                } else {
                    EnforcerTransition transSecond = new EnforcerTransition(targetMain, mapStateEnf.get(propTran.target), true);
                    Set <EnforcerTransition> transSeconds = new HashSet<EnforcerTransition>();
                    transSeconds.add(transSecond);
                    eccOut.put(targetMain, transSeconds);
                }
            } else if (propTran.type.equals("BUFFER")) {

                for (String data : propTran.data) {
                    tmpEnfAlgo += "Buf_" + data + "["+ "Buf_" + propTran.event +"] := " + data + "_I; ";
                }
                tmpEnfAlgo += "Buf_" + propTran.event + " := Buf_" + propTran.event + " + 1;";
                Set <EnforcerAlgo> tmpExistAlgo = new HashSet<EnforcerAlgo>();
                for (EnforcerAlgo enforcerAlgo : algosOut) {
                    if (enforcerAlgo.algo.equals(tmpEnfAlgo)) {
                        tmpExistAlgo.add(enforcerAlgo);
                    }
                }
                EnforcerAlgo newEnfAlgo;
                if (tmpExistAlgo.size() == 0) {
                    newEnfAlgo = new EnforcerAlgo(tmpEnfAlgo);
                    algosOut.add(newEnfAlgo);
                } else {
                    Iterator <EnforcerAlgo> itAlgo = tmpExistAlgo.iterator();
                    newEnfAlgo = itAlgo.next();
                }
                EnforcerAction act = new EnforcerAction(newEnfAlgo.ID, "");
                targetActs.add(act);
                
                EnforcerState targetMain = new EnforcerState(targetActs);
                EnforcerTransition transMain = new EnforcerTransition(cStateEnf, transEvIn, guardString, propTran.guardString, targetMain, false);
                Set <EnforcerTransition> transMains = new HashSet<EnforcerTransition>();
                if (eccOut.containsKey(cStateEnf)) {
                    transMains.addAll(eccOut.get(cStateEnf));
                }
                transMains.add(transMain);
                eccOut.put(cStateEnf, transMains);
                List <EnforcerAction> targetActsS = new ArrayList<EnforcerAction>();
                if (!visited.contains(propTran.target)) {
                    EnforcerState targetSecond = new EnforcerState(targetActsS);
                    EnforcerTransition transSecond = new EnforcerTransition(targetMain, targetSecond, true);
                    Set <EnforcerTransition> transSeconds = new HashSet<EnforcerTransition>();
                    transSeconds.add(transSecond);
                    eccOut.put(targetMain, transSeconds);
                    buildAllECC(propMapIn, targetSecond, propTran.target, eccOut, visited, mapStateEnf, algosOut);
                } else {
                    EnforcerTransition transSecond = new EnforcerTransition(targetMain, mapStateEnf.get(propTran.target), true);
                    Set <EnforcerTransition> transSeconds = new HashSet<EnforcerTransition>();
                    transSeconds.add(transSecond);
                    eccOut.put(targetMain, transSeconds);
                }
            }
        }
    }

    static void buildRelease(Map <Integer, Set <Transition>> propMapIn, EnforcerState cStateEnf, int cState,
    Map <EnforcerState, Set <EnforcerTransition>> eccOut, Set <Integer> visited, Map <Integer, EnforcerState> mapStateEnf,
    Set <EnforcerAlgo> algosOut) {
        visited.add(cState);
        for (Transition propTran : propMapIn.get(cState)) {
            if (propTran.tag.equals("release1")) {
                String guardString = propTran.guardString.replaceAll(" = ", "_I = ")
                .replaceAll(" != ", "_I != ")
                .replaceAll(" <= ", "_I <= ")
                .replaceAll(" >= ", "_I >= ")
                .replaceAll(" > ", "_I > ")
                .replaceAll(" < ", "_I < ");
                List <EnforcerAction> targetActs = new ArrayList<EnforcerAction>();
                String transEvIn = propTran.event + "_I";
                String tmpEnfAlgo = "";
                String tmpGuard = "";
                if (propTran.type.equals("FORWARD")) {
                    Iterator <String> it = propTran.guard.iterator();
                    while (it.hasNext()) { 
                        tmpGuard = it.next(); 
                        if (tmpGuard.contains("!=")) {
                            tmpEnfAlgo += tmpGuard.split("!=")[0] + "_O := " + tmpGuard.split("!=")[0] + "_I; ";
                        } else if (tmpGuard.contains("<=")) {
                            tmpEnfAlgo += tmpGuard.split("<=")[0] + "_O := " + tmpGuard.split("<=")[0] + "_I; ";
                        } else if (tmpGuard.contains(">=")) {
                            tmpEnfAlgo += tmpGuard.split(">=")[0] + "_O := " + tmpGuard.split(">=")[0] + "_I; ";
                        } else if (tmpGuard.contains("=")) {
                            tmpEnfAlgo += tmpGuard.split("=")[0] + "_O := " + tmpGuard.split("=")[0] + "_I; ";
                        } else if (tmpGuard.contains("<")) {
                            tmpEnfAlgo += tmpGuard.split("<")[0] + "_O := " + tmpGuard.split("<")[0] + "_I; ";
                        } else if (tmpGuard.contains(">")) {
                            tmpEnfAlgo += tmpGuard.split(">")[0] + "_O := " + tmpGuard.split(">")[0] + "_I; ";
                        }
                    }
                    Set <EnforcerAlgo> tmpExistAlgo = new HashSet<EnforcerAlgo>();
                    for (EnforcerAlgo enforcerAlgo : algosOut) {
                        if (enforcerAlgo.algo.equals(tmpEnfAlgo)) {
                            tmpExistAlgo.add(enforcerAlgo);
                        }
                    }
                    EnforcerAlgo newEnfAlgo;
                    String targetEvOut = propTran.event + "_O";
                    EnforcerAction targetAct;
                    if (tmpEnfAlgo.length() > 0) {
                        if (tmpExistAlgo.size() == 0) {
                            newEnfAlgo = new EnforcerAlgo(tmpEnfAlgo);
                            algosOut.add(newEnfAlgo);
                        } else {
                            Iterator <EnforcerAlgo> itAlgo = tmpExistAlgo.iterator();
                            newEnfAlgo = itAlgo.next();
                        }
                        targetAct = new EnforcerAction(newEnfAlgo.ID, targetEvOut);
                    } else {
                        targetAct = new EnforcerAction("", targetEvOut);
                    }
                    targetActs.add(targetAct);
                } else if (propTran.type.equals("DISCARD")) {
                    
                } else if (propTran.type.equals("REPLACE")) {
                    Iterator <String> it = propTran.repGuard.iterator();
                    while (it.hasNext()) { 
                        tmpGuard = it.next(); 
                        tmpEnfAlgo += tmpGuard.split("=")[0] + "_O := " + tmpGuard.split("=")[1] + "; ";
                    }
                    Set <EnforcerAlgo> tmpExistAlgo = new HashSet<EnforcerAlgo>();
                    for (EnforcerAlgo enforcerAlgo : algosOut) {
                        if (enforcerAlgo.algo.equals(tmpEnfAlgo)) {
                            tmpExistAlgo.add(enforcerAlgo);
                        }
                    }
                    EnforcerAlgo newEnfAlgo;
                    String targetEvOut = propTran.repEvent + "_O";
                    EnforcerAction targetAct;
                    if (tmpEnfAlgo.length() > 0) {
                        if (tmpExistAlgo.size() == 0) {
                            newEnfAlgo = new EnforcerAlgo(tmpEnfAlgo);
                            algosOut.add(newEnfAlgo);
                        } else {
                            Iterator <EnforcerAlgo> itAlgo = tmpExistAlgo.iterator();
                            newEnfAlgo = itAlgo.next();
                        }
                        targetAct = new EnforcerAction(newEnfAlgo.ID, targetEvOut);
                    } else {
                        targetAct = new EnforcerAction("", targetEvOut);
                    }
                    targetActs.add(targetAct);
                } else if (propTran.type.equals("BUFFER")) {
                    for (String data : propTran.data) {
                        tmpEnfAlgo += "Buf_" + data + "["+ "Buf_" + propTran.event +"] := " + data + "_I; ";
                    }
                    tmpEnfAlgo += "Buf_" + propTran.event + " := Buf_" + propTran.event + " + 1;";
                    Set <EnforcerAlgo> tmpExistAlgo = new HashSet<EnforcerAlgo>();
                    for (EnforcerAlgo enforcerAlgo : algosOut) {
                        if (enforcerAlgo.algo.equals(tmpEnfAlgo)) {
                            tmpExistAlgo.add(enforcerAlgo);
                        }
                    }
                    EnforcerAlgo newEnfAlgo;
                    if (tmpExistAlgo.size() == 0) {
                        newEnfAlgo = new EnforcerAlgo(tmpEnfAlgo);
                        algosOut.add(newEnfAlgo);
                    } else {
                        Iterator <EnforcerAlgo> itAlgo = tmpExistAlgo.iterator();
                        newEnfAlgo = itAlgo.next();
                    }
                    EnforcerAction act = new EnforcerAction(newEnfAlgo.ID, "");
                    targetActs.add(act);
                }
                for (Transition tranRelease : propMapIn.get(propTran.target)) {
                    if (tranRelease.tag.equals("release2")) {
                        String mainGuardString = ""  + guardString;
                        // if (propTran.toBuf.size() == 1) {
                        //     if (guardString.equals("")) {
                        //         mainGuardString += "Buf_" +tranRelease.event+ " > 0" ;
                        //     } else {
                        //         mainGuardString += " AND Buf_" +tranRelease.event+ " > 0" ;
                        //     }
                        // } else {
                        //     for (String tb : propTran.toBuf) {
                        //         if (!tb.equals(tranRelease.event)) {
                        //             if (guardString.equals("")) {
                        //                 mainGuardString += "Buf_" +tranRelease.event+ " > " + "Buf_" +tb;
                        //             } else {
                        //                 mainGuardString += " AND Buf_" +tranRelease.event+ " > " + "Buf_" +tb;
                        //             }
                        //         }   
                        //     }
                        // }
                        if (guardString.equals("")) {
                            mainGuardString += "Buf_" +tranRelease.event+ " > 0" ;
                        } else {
                            mainGuardString += " AND Buf_" +tranRelease.event+ " > 0" ;
                        }
                        List <EnforcerAction> mainTargetActs = new ArrayList<EnforcerAction>();
                        mainTargetActs.addAll(targetActs);
                        String releaseAlgo = "";
                        for (String data : tranRelease.data) {
                            releaseAlgo += data + "_O := " + "Buf_" + data + "["+ "Buf_" + tranRelease.event +"]; ";
                        }
                        releaseAlgo += "Buf_" + tranRelease.event + " := Buf_" + tranRelease.event + " - 1;";
                        Set <EnforcerAlgo> tmpExistAlgoRelease = new HashSet<EnforcerAlgo>();
                        for (EnforcerAlgo enforcerAlgo : algosOut) {
                            if (enforcerAlgo.algo.equals(releaseAlgo)) {
                                tmpExistAlgoRelease.add(enforcerAlgo);
                            }
                        }
                        EnforcerAlgo newEnfAlgoRelease;
                        if (tmpExistAlgoRelease.size() == 0) {
                            newEnfAlgoRelease = new EnforcerAlgo(releaseAlgo);
                            algosOut.add(newEnfAlgoRelease);
                        } else {
                            Iterator <EnforcerAlgo> itAlgo = tmpExistAlgoRelease.iterator();
                            newEnfAlgoRelease = itAlgo.next();
                        }
                        String releaseEvent =  tranRelease.event + "_O";
                        EnforcerAction releaseAct = new EnforcerAction(newEnfAlgoRelease.ID, releaseEvent);
                        mainTargetActs.add(releaseAct);
                        EnforcerState targetMain = new EnforcerState(mainTargetActs);
                        EnforcerTransition transMain = new EnforcerTransition(cStateEnf, transEvIn, mainGuardString, propTran.guardString, targetMain, false);
                        Set <EnforcerTransition> transMains = new HashSet<EnforcerTransition>();
                        if (eccOut.containsKey(cStateEnf)) {
                            transMains.addAll(eccOut.get(cStateEnf));
                        }
                        transMains.add(transMain);
                        eccOut.put(cStateEnf, transMains);
                        EnforcerTransition transSecond = new EnforcerTransition(targetMain, mapStateEnf.get(tranRelease.target), true);
                        Set <EnforcerTransition> transSeconds = new HashSet<EnforcerTransition>();
                        transSeconds.add(transSecond);
                        eccOut.put(targetMain, transSeconds);
                    }
                }
            }
            if (!visited.contains(propTran.target)) {
                buildRelease(propMapIn, mapStateEnf.get(propTran.target), propTran.target, eccOut, visited, mapStateEnf, algosOut);
            }
        }
        
    }

    static void printInterfaces (String name, Set <String> itfs) {
        System.out.print("\t" + name + " = {");
        int tmplen = itfs.size();
        for (String itfstr : itfs) {
            System.out.print(itfstr);
            tmplen--;
            if (tmplen != 0) {
                System.out.print(", ");
            }
        }
        System.out.println("}");
    }
    static void printDataInterfaces (String name, Set <dataItf> itfs) {
        System.out.print("\t" + name + " = {");
        int tmplen = itfs.size();
        for (dataItf itfstr : itfs) {
            System.out.print(itfstr.name);
            tmplen--;
            if (tmplen != 0) {
                System.out.print(", ");
            }
        }
        System.out.println("}");
    }

    static void printWiths (String name, Set <With> wths) {
        System.out.print("\t" + name + " = {");
        int tmplen = wths.size();
        for (With wth : wths) {
            System.out.print(wth.event +  " - " + wth.data);
            tmplen--;
            if (tmplen != 0) {
                System.out.print(", ");
            }
        }
        System.out.println("}");
    }

    static void printECC (Map <EnforcerState, Set <EnforcerTransition>> ecc) {
        System.out.println("\nECC: ");
        for (EnforcerState st : ecc.keySet()) {
            System.out.println("\tState " + st.state);
            System.out.println("\t\tActions:");
            for (EnforcerAction ac : st.actions) {
                System.out.println("\t\t\tAlgo: " + ac.algoID + ", Event: " + ac.eventOut);
            }
            System.out.println("\t\tTransition:");
            for (EnforcerTransition tr : ecc.get(st)) {
                System.out.print("\t\t\tsrc: " + tr.source.state + ", Label: ");
                if (!tr.event.equals("")) {
                    if (tr.guardString.length()>0) {
                        System.out.print(tr.event + "["+tr.guardString+"]");
                    } else {
                        System.out.print(tr.event);
                    }
                } else {
                    System.out.print("1");
                }
                System.out.println(", tgt: " + tr.target.state);
            } 
        }
    }

    static void printAlgos (Set <EnforcerAlgo> algos) {
        System.out.println("\nAlgorithms:");
        for (EnforcerAlgo alg: algos) {
            System.out.print("\t" + alg.ID);
            System.out.println(": | " + alg.algo + " |");
        }
    }

    static void printProp (Map <Integer, Set <Transition>> propMap) {
        for (Integer st : propMap.keySet()) {
            System.out.println("\n\tState " + st);
            for (Transition tr : propMap.get(st)) {
                if (tr.guardString.length() > 0) {
                    System.out.print("\t\tSrc: " + tr.source + ", Label: " + tr.event + "[" +tr.guardString+"]");
                } else {
                    System.out.print("\t\tSrc: " + tr.source + ", Label: " + tr.event);
                }
                System.out.print(", Tgt: " + tr.target);
                System.out.print(", Type: " + tr.type);
                if (tr.repEvent.length()>0) {
                    if (tr.repGuardString.length()>0) {
                        System.out.print(" (" + tr.repEvent + "["+ tr.repGuardString +"])");
                    } else {
                        System.out.print(" (" + tr.repEvent + ")");
                    }
                }
                System.out.print(", Tag: " + tr.tag);
                if (tr.toBuf.size() > 0){
                    System.out.print(", buffered: ");
                    for (String tb : tr.toBuf) {
                        System.out.print(tb + " ");
                    }
                }
                System.out.println();
            }
        }
    }

    static void printXML (EnforcerFB efb, Set <iVariables> iVars) {
        try {
            File inputFile = new File("TEMPLATE_1.fbt");
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            Document doc = docBuilder.parse(inputFile);
   
            // update FB name attribute
            Node type = doc.getElementsByTagName("FBType").item(0);
            NamedNodeMap attr = type.getAttributes();
            Node nodeAttr = attr.getNamedItem("Name");
            nodeAttr.setTextContent(efb.name);
            nodeAttr = attr.getNamedItem("Comment");
            nodeAttr.setTextContent("This is an enforcer FB");
   
            // add interfaces
            Element root = (Element) doc.getElementsByTagName("EventInputs").item(0);
            List<String> listEvIn = new ArrayList<String>();
            listEvIn.addAll(efb.interfaces.eventInputs);
            Collections.sort(listEvIn);
            for (String evItf : listEvIn) {
                Element newEvent = doc.createElement("Event");
                newEvent.setAttribute("Name", evItf);
                newEvent.setAttribute("Type", "Event");
                newEvent.setAttribute("Comment", "");
                for (With wth: efb.interfaces.withInputs) {
                    if (wth.event.equals(evItf)) {
                        Element newWith = doc.createElement("With");
                        newWith.setAttribute("Var", wth.data);
                        newEvent.appendChild(newWith);
                    }
                }
                root.appendChild(newEvent);
            }
            root = (Element) doc.getElementsByTagName("EventOutputs").item(0);
            List<String> listEvOut = new ArrayList<String>();
            listEvOut.addAll(efb.interfaces.eventOutputs);
            Collections.sort(listEvOut);
            for (String evItf : listEvOut) {
                Element newEvent = doc.createElement("Event");
                newEvent.setAttribute("Name", evItf);
                newEvent.setAttribute("Type", "Event");
                newEvent.setAttribute("Comment", "");
                for (With wth: efb.interfaces.withOutputs) {
                    if (wth.event.equals(evItf)) {
                        Element newWith = doc.createElement("With");
                        newWith.setAttribute("Var", wth.data);
                        newEvent.appendChild(newWith);
                    }
                }
                root.appendChild(newEvent);
            }
            root = (Element) doc.getElementsByTagName("InputVars").item(0);
            List<dataItf> listInItf = new ArrayList<dataItf>();
            listInItf.addAll(efb.interfaces.dataInputs);
            Collections.sort(listInItf, (o1, o2) -> (o1.name.compareTo(o2.name)));
            for (dataItf dtItf : listInItf) {
                Element newData = doc.createElement("VarDeclaration");
                newData.setAttribute("Name", dtItf.name);
                newData.setAttribute("Type", dtItf.type);
                newData.setAttribute("Comment", "");
                root.appendChild(newData);
            }
            root = (Element) doc.getElementsByTagName("OutputVars").item(0);
            List<dataItf> listOutItf = new ArrayList<dataItf>();
            listOutItf.addAll(efb.interfaces.dataOutputs);
            Collections.sort(listOutItf, (o1, o2) -> (o1.name.compareTo(o2.name)));
            for (dataItf dtItf : listOutItf) {
                Element newData = doc.createElement("VarDeclaration");
                newData.setAttribute("Name", dtItf.name);
                newData.setAttribute("Type", dtItf.type);
                newData.setAttribute("Comment", "");
                root.appendChild(newData);
            }

            // add Internal Vars
            root = (Element) doc.getElementsByTagName("InternalVars").item(0);
            for (iVariables iVar : iVars) {
                Element newData = doc.createElement("VarDeclaration");
                newData.setAttribute("Name", iVar.name);
                newData.setAttribute("Type", iVar.type);
                newData.setAttribute("Comment", "");
                if (iVar.isEvent == true) {
                    newData.setAttribute("InitialValue", "0");
                } else {
                    newData.setAttribute("ArraySize", "9999");   
                }
                root.appendChild(newData);
            }

            // add ECC
            root = (Element) doc.getElementsByTagName("ECC").item(0);
            double xCoor = 0.0;
            List<EnforcerState> listOfState = new ArrayList<EnforcerState>();
            listOfState.addAll(efb.ecc.keySet());
            Collections.sort(listOfState, (o1, o2) -> o1.state - o2.state);
            for (EnforcerState efState : listOfState) {
                Element newState = doc.createElement("ECState");
                newState.setAttribute("Name", "S"+efState.state);
                newState.setAttribute("x", "" + xCoor);
                newState.setAttribute("y", "" + xCoor);
                if (efState.state == 0) {
                    newState.setAttribute("Comment", "Initial State");
                } else {
                    newState.setAttribute("Comment", "");
                }
                if (efState.actions.size()>0) {
                    for (EnforcerAction efAction : efState.actions) {
                        Element newAction = doc.createElement("ECAction");
                        newAction.setAttribute("Algorithm", efAction.algoID);
                        newAction.setAttribute("Output", efAction.eventOut);
                        newState.appendChild(newAction);
                    }
                }
                root.appendChild(newState);
                xCoor = xCoor + 300.0;
            }
            xCoor = 0.0;
            for (EnforcerState efState : efb.ecc.keySet()) {
                for (EnforcerTransition efTrans  : efb.ecc.get(efState)) {
                    Element newTrans = doc.createElement("ECTransition");
                    newTrans.setAttribute("Source", "S"+efTrans.source.state);
                    newTrans.setAttribute("Destination", "S"+efTrans.target.state);
                    if (efTrans.event.length() > 0) {
                        newTrans.setAttribute("Condition", efTrans.event + "[" + efTrans.guardString + "]");
                    } else {
                        newTrans.setAttribute("Condition", "1");
                    }
                    
                    newTrans.setAttribute("Comments", "");
                    newTrans.setAttribute("x", ""+xCoor);
                    newTrans.setAttribute("y", ""+xCoor);
                    xCoor = xCoor + 150.0;
                    root.appendChild(newTrans);
                }
            }
            
            // add algorithm
            root = (Element) doc.getElementsByTagName("BasicFB").item(0);
            for (EnforcerAlgo algo : efb.algorithms) {
                Element newAlgo = doc.createElement("Algorithm");
                newAlgo.setAttribute("Name", algo.ID);
                newAlgo.setAttribute("Comment", "");
                Element newST = doc.createElement("ST");
                Node cdata = doc.createCDATASection(algo.algo);
                newST.appendChild(cdata);
                newAlgo.appendChild(newST);
                root.appendChild(newAlgo);
            }

            // remove spaces
            XPath xp = XPathFactory.newInstance().newXPath();
            NodeList nl = (NodeList) xp.evaluate("//text()[normalize-space(.)='']", doc, XPathConstants.NODESET);
            for (int i=0; i < nl.getLength(); ++i) {
               Node node = nl.item(i);
               node.getParentNode().removeChild(node);
            }
   
            // write the content to new xml
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            DOMSource source = new DOMSource(doc);
            System.out.println("\nOutput Enforcer FB: " + efb.name + ".fbt");
            StreamResult consoleResult = new StreamResult(new File(efb.name+".fbt"));
            transformer.transform(source, consoleResult);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws FileNotFoundException, IOException {
        String fileName = args[0];
        String propMeta = "";
        int tmpSource = 0;
        String tmpEvent = "";
        Set <String> tmpGuards = new HashSet<String>();
        String tmpGuardString = "";
        int tmpTarget = 0;
        String tmpType = "";
        String tmpRepEvent = "";
        Set <String> tmpRepGuards = new HashSet<String>();
        String [] tmpLabel;
        String [] tmpRepLabel;
        String tmpRepGuardString = "";
        Set <Transition> tmpTrans;
        String[] arrLines;
        Set <iVariables> iVars = new HashSet<iVariables>();
        String spliter = "";
        Set <String> tmpVars = new HashSet<String>();
        String tmpRelease = "";
        String tmpTag = "";
        Set <String> tmptoBuf = new HashSet<String>();
        // Input property
        Map <Integer, Set <Transition>> propMap = new HashMap <Integer, Set <Transition>>();
        try(BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String line = br.readLine();
            propMeta = line;
            while (true) {
                line = br.readLine();
                if (line != null){
                    if (line.split("\\|").length > 1) {
                        tmpRelease = line.split("\\|")[1].replace(" ", "");
                    }
                    line = line.split("\\|")[0];
                    arrLines = line.split(",");
                    tmpLabel = arrLines[1].split("\\.");
                    tmpSource = Integer.parseInt(arrLines[0]);
                    tmpEvent = tmpLabel[0].split("\\[")[0].replace(" ", "");
                    tmpGuards = new HashSet<String>();
                    tmpGuardString = "";
                    if (tmpLabel[0].split("\\[").length>1) {
                        tmpGuardString = tmpLabel[0].split("\\[")[1].replace("]", "");
                        tmpGuards.addAll(
                            new HashSet<>(
                                Arrays.asList(
                                    (tmpGuardString.replace("(", "").replace(")", "").replace(" ", "")).split("AND|OR"))));
                    }
                    // aaa;
                    tmpTarget = Integer.parseInt(arrLines[2].replace(" ", ""));
                    tmpTrans = new HashSet<Transition>();         
                    if (propMap.containsKey(tmpSource)) {
                        tmpTrans.addAll(propMap.get(tmpSource));
                    }
                    tmpType = "";
                    tmpType = tmpLabel[1].replace(" ", "");
                    if (tmpType.equals("BUFFER")) {
                        iVars.add(new iVariables("Buf_"+tmpEvent, "INT", true));
                    }
                    tmpVars = new HashSet<String>();
                    for (String gd : tmpGuards) {
                        if (gd.contains("!=")) {
                            spliter = "!=";
                        } else if (gd.contains("<=")) {
                            spliter = "<=";
                        } else if (gd.contains(">=")) {
                            spliter = ">=";
                        } else if (gd.contains("=")) {
                            spliter = "=";
                        } else if (gd.contains("<")) {
                            spliter = "<";
                        } else if (gd.contains(">")) {
                            spliter = ">";
                        }
                        if (tmpType.equals("BUFFER")) {
                            if (gd.split(spliter)[1].equals("true") || gd.split(spliter)[1].equals("false")) {
                                iVars.add(new iVariables("Buf_"+gd.split(spliter)[0], "BOOL", false));
                            } else {
                                iVars.add(new iVariables("Buf_"+gd.split(spliter)[0], "INT", false));
                            }
                        }
                        tmpVars.add(gd.split(spliter)[0]);
                    }

                    tmpRepEvent = "";
                    tmpRepGuards = new HashSet<String>();
                    tmpRepGuardString = "";
                    if (tmpLabel.length > 2) {
                        tmpRepLabel = tmpLabel[2].split("\\[");
                        tmpRepEvent = tmpRepLabel[0].replace(" ", "");
                        if(tmpLabel[2].split("\\[").length > 1) {
                            tmpRepGuardString = tmpRepLabel[1].replace("]", "");
                            tmpRepGuards.addAll(
                            new HashSet<>(
                                Arrays.asList(
                                    (tmpRepGuardString.replace("(", "").replace(")", "").replace(" ", "")).split("AND|OR"))));
                        }
                    }
                    tmptoBuf = new HashSet<String>();
                    tmpTag = "";
                    if (tmpRelease.split("\\(").length > 1) {
                        tmpTag = "release1";
                        tmptoBuf.addAll(
                        new HashSet<>(Arrays.asList(
                            tmpRelease.split("\\(")[1].replace(" ", "").replace(")", "").split(","))));
                    } else if (tmpRelease.replace(" ", "").equals("release2")){
                        tmpTag = "release2";
                    }

                    tmpTrans.add(new Transition(tmpSource, tmpEvent, tmpGuards, tmpGuardString, tmpTarget, tmpType, tmpRepEvent, tmpRepGuards, tmpRepGuardString, tmpVars, tmpTag, tmptoBuf));
                    propMap.put(tmpSource, tmpTrans);

                    if(!propMap.containsKey(tmpTarget)) {
                        propMap.put(tmpTarget, new HashSet<Transition>());
                    }
                } else { break; }
            }
        }

        System.out.println("============================================\nINPUT\n");
        System.out.println("Property Name: " + propMeta);
        printProp(propMap);

        // Build Interfaces
        Set <String> myinputevents = new HashSet<String>();
        Set <String> myoutputevents = new HashSet<String>();
        Set <dataItf> myinputdata = new HashSet<dataItf>();
        Set <dataItf> myoutputdata = new HashSet<dataItf>();
        Set <With> mywithinput = new HashSet<With>();
        Set <With> mywithoutput = new HashSet<With>();
        
        for (Integer st : propMap.keySet()) {
            for (Transition tr : propMap.get(st)) {
                myinputevents.add(tr.event+"_I");
                myoutputevents.add(tr.event+"_O");

                for (String gd : tr.guard) {
                    if (gd.contains("!=")) {
                        spliter = "!=";
                    } else if (gd.contains("<=")) {
                        spliter = "<=";
                    } else if (gd.contains(">=")) {
                        spliter = ">=";
                    } else if (gd.contains("=")) {
                        spliter = "=";
                    } else if (gd.contains("<")) {
                        spliter = "<";
                    } else if (gd.contains(">")) {
                        spliter = ">";
                    }
                    if (gd.split(spliter)[1].equals("true") || gd.split(spliter)[1].equals("false")) {
                        myinputdata.add(new dataItf(gd.split(spliter)[0]+"_I", "BOOL"));
                        myoutputdata.add(new dataItf(gd.split(spliter)[0]+"_O", "BOOL"));
                    } else {
                        myinputdata.add(new dataItf(gd.split(spliter)[0]+"_I", "INT"));
                        myoutputdata.add(new dataItf(gd.split(spliter)[0]+"_O", "INT"));
                    }
                    mywithinput.add(new With(tr.event+"_I", gd.split(spliter)[0]+"_I"));
                    mywithoutput.add(new With(tr.event+"_O", gd.split(spliter)[0]+"_O"));
                }

                if (tr.repEvent.length() > 0) {
                    myoutputevents.add(tr.repEvent+"_O");
                }
                for (String gd : tr.repGuard) {
                    spliter = "=";
                    if (gd.split(spliter)[1].equals("true") || gd.split(spliter)[1].equals("false")) {
                        myoutputdata.add(new dataItf(gd.split(spliter)[0]+"_O", "BOOL"));
                    } else {
                        myoutputdata.add(new dataItf(gd.split(spliter)[0]+"_O", "INT"));
                    }
                    mywithoutput.add(new With(tr.event+"_O", gd.split(spliter)[0]+"_O"));
                }
            }
        }
        EnforcerInterfaces myinterfaces = new EnforcerInterfaces (
            myinputevents, myoutputevents, myinputdata, myoutputdata,
            mywithinput, mywithoutput);
        
        // Build ECC and algos
        Map <EnforcerState, Set <EnforcerTransition>> ecc = new HashMap <EnforcerState, Set <EnforcerTransition>>();
        Set <EnforcerAlgo> algos = new HashSet<EnforcerAlgo>();
        Map <Integer, EnforcerState> mapStates =  new HashMap<Integer, EnforcerState>();
        buildAllECC(propMap, new EnforcerState(new ArrayList<EnforcerAction>()), 0, ecc, new HashSet<Integer>(), mapStates, algos);
        buildRelease(propMap, mapStates.get(0), 0, ecc, new HashSet<Integer>(), mapStates, algos);
        // buildECCForward(propMap, new EnforcerState(new HashSet<EnforcerAction>()), 0, ecc, new HashSet<Integer>(), mapStates, algos);
        // buildECCReplace(propMap, ecc, mapStates, algos);
        // buildECCBuffer(propMap, ecc, mapStates, algos, iVars);
        EnforcerFB enforcerFB = new EnforcerFB(propMeta, myinterfaces, ecc, algos);
        
        // prints
        System.out.println("\n============================================\nOUTPUT\n");
        System.out.println("Enforcer FB Name: " + enforcerFB.name);
        System.out.println("\nInterfaces:");
        printInterfaces("Input Events", enforcerFB.interfaces.eventInputs);
        printInterfaces("Output Events", enforcerFB.interfaces.eventOutputs);
        printDataInterfaces("Input Data", enforcerFB.interfaces.dataInputs);
        printDataInterfaces("Output Data", enforcerFB.interfaces.dataOutputs);
        printWiths("Input With", enforcerFB.interfaces.withInputs);
        printWiths("Output With", enforcerFB.interfaces.withOutputs);
        System.out.println("\nState mapping");
        for (Integer st : mapStates.keySet()) {
            System.out.println("\tProp state: "+st+", ECC state: "+mapStates.get(st).state);
        }
        System.out.println("\nInternal Variables: ");
        for (iVariables iVar : iVars) {
            System.out.println("\tName: " + iVar.name + ", Type: " + iVar.type);
        }
        printECC(enforcerFB.ecc);
        printAlgos(enforcerFB.algorithms);
        printXML(enforcerFB, iVars);
    }
}